//
//  Extensions.swift
//  AirportLocator
//
//  Created by Abdul on 31/10/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import Foundation
import UIKit

// MARK: - URLComponents Extension

extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: String]) {
        self .queryItems = parameters .map {
            URLQueryItem(name: $0.key, value: $0.value)
        }
    }
}

// MARK: - NSDictionary Extension

extension NSDictionary {
    
    func hasKey() -> Bool {
        return self .allKeys .count > 0 ? true : false
    }
}

// MARK: - Color Extension

extension UIColor {
    
    static let colorAppTheme: UIColor = UIColor(red: 227.0 / 255.0,
                                                green: 37.0 / 255.0,
                                                blue: 38.0 / 255.0, alpha: 1.0)
}

// MARK: - Notification Extension

extension Notification .Name {
    static let locationAccess = Notification .Name("locationAccess")
}

// MARK: - String Extension

extension String {
    
    static let sAppName: String = Bundle .main .infoDictionary![kCFBundleNameKey as String] as! String
    static let sGoogleAPIKey: String = "AIzaSyASH7e-Ul4xSKvUEDSQQlFlaskNmd4ZzxE"
    
    static let sAPIURLScheme: String = "https"
    static let sAPIURLHost: String = "maps.googleapis.com"
    static let sAPIAirportPath: String = "/maps/api/place/nearbysearch/json"
    
    static let sAirportQueryParamLocationKey: String = "location"
    static let sAirportQueryParamRadiusKey: String = "radius"
    static let sAirportQueryParamTypeKey: String = "type"
    static let sAirportQueryParamKeywordKey: String = "keyword"
    static let sAirportQueryParamKey: String = "key"
    static let sAirportQueryParamValue: String = "airport"
    
    static let sAlertNoInternet: String = "No network connection ⚠️"
    static let sAlertAPIFailed: String = "Something went wrong. Try again later! ☹️"
    static let sAlertAPISuccess: String = "Success! 👍🏻"
    static let sEnableLocationAccess: String = "Please enable location access under device settings 🛠"
    
    static let keyLatitude: String = "keyLat"
    static let keyLongitude: String = "keyLon"
    static let keyisLocationEnabled: String = "keyLocationStatus"
}
