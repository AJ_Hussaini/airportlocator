//
//  MapArtWork.swift
//  AirportLocator
//
//  Created by Abdul on 02/11/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class MapArtWork: NSObject, MKAnnotation {
    
    // MARK: - Identifiers
    var locationName: String?
    var locationIcon: String?
    var coordinate: CLLocationCoordinate2D
    var title: String? {
        return locationName
    }
    var subtitle: String? {
        // Calculate distance between coordinates
        let userLocation = CLLocation(latitude: Constants .sharedInstance .getValueFromUserDefaults(key: .keyLatitude) as! CLLocationDegrees, longitude: Constants .sharedInstance .getValueFromUserDefaults(key: .keyLongitude) as! CLLocationDegrees)
        let mapArtLocation = CLLocation(latitude: coordinate .latitude, longitude: coordinate .longitude)
        let distance = mapArtLocation .distance(from: userLocation)
        return "\(Int64(distance / 1000.0)) KM"
    }

    // MARK: - Initializer
    init(locationName: String? = nil,
         locationIcon: String? = nil,
         coordinate: CLLocationCoordinate2D? = nil) {
        
        self .locationName = locationName ?? ""
        self .locationIcon = locationIcon ?? ""
        self .coordinate = coordinate ?? CLLocationCoordinate2D()
    }
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem .name = title
        return mapItem
    }
}
