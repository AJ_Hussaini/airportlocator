//
//  LocationManagerHandler.swift
//  AirportLocator
//
//  Created by Abdul on 31/10/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManagerHandler: NSObject, CLLocationManagerDelegate {
    
    // MARK: - Shared Instance Declaration
    static let sharedInstance = LocationManagerHandler()
    
    // MARK: - Identifiers
    let locationManager = CLLocationManager()
    
    // MARK: - CLLocationManager Handlers
    func getCurrentLocation() {
        
        let status = CLLocationManager .authorizationStatus()
        locationManager .delegate = self
        locationManager .startUpdatingLocation()

        switch status {
        case .notDetermined:
            locationManager .requestWhenInUseAuthorization()
            return
            
        case .denied, .restricted:
            Constants .sharedInstance .setValueToUserDefaults(key: .keyisLocationEnabled, value: false)

            let alert = UIAlertController(title: "Location Services disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert .addAction(okAction)
            let settingsActionButton: UIAlertAction = UIAlertAction(title: "Settings", style: .default) { action -> Void in
                UIApplication .shared .open(URL(string:UIApplication .openSettingsURLString)!)
            }
            alert.addAction(settingsActionButton)
            Constants .sharedInstance .getTopMostViewController()? .present(alert, animated: true, completion: nil)
            break
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager .startUpdatingLocation()
            break

        default:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations .last {
            //  Store latitude and longitude in local cache
            locationManager .stopUpdatingLocation()
            Constants .sharedInstance .setValueToUserDefaults(key: .keyLatitude, value: currentLocation .coordinate.latitude)
            Constants .sharedInstance .setValueToUserDefaults(key: .keyLongitude, value: currentLocation .coordinate.longitude)
            
            let isLocationEnable = Constants .sharedInstance .getValueFromUserDefaults(key: .keyisLocationEnabled) as? Bool ?? true
            Constants .sharedInstance .setValueToUserDefaults(key: .keyisLocationEnabled, value: isLocationEnable)
            
            NotificationCenter .default .post(name: .locationAccess, object: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let appDelegate = UIApplication .shared .delegate as? AppDelegate {
            if appDelegate .statusLocationAccess != "failure" {
                appDelegate .statusLocationAccess = "failure"
                Constants .sharedInstance .setValueToUserDefaults(key: .keyisLocationEnabled, value: false)
                NotificationCenter .default .post(name: .locationAccess, object: nil)
                getCurrentLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if let appDelegate = UIApplication .shared .delegate as? AppDelegate {
            if appDelegate .statusLocationAccess == "failure" && status == .authorizedWhenInUse || status == .authorizedAlways {
                Constants .sharedInstance .setValueToUserDefaults(key: .keyisLocationEnabled, value: true)
                getCurrentLocation()
            } else {
                locationManager .startUpdatingLocation()
            }
        }
    }
}
