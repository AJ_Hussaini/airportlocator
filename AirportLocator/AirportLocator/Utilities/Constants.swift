//
//  Constants.swift
//  AirportLocator
//
//  Created by Abdul on 02/11/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    // MARK: - Shared Instance Declaration
    static let sharedInstance = Constants()
    
    // MARK: - UserDefaults Handlers
    func setValueToUserDefaults(key: String,
                                value: Any) {
        let userDefaults: UserDefaults = UserDefaults .standard
        userDefaults .set(value, forKey: key)
        userDefaults .synchronize()
    }
    
    func getValueFromUserDefaults(key: String) -> Any {
        let userDefaults: UserDefaults = UserDefaults .standard
        return userDefaults .value(forKey: key) ?? ""
    }
    
    // MARK: - View Controller Stack Handler
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow? .rootViewController
        while let presentedViewController = topMostViewController? .presentedViewController {
            topMostViewController = presentedViewController
        }
        return topMostViewController
    }
}
