//
//  WebserviceHandler.swift
//  AirportLocator
//
//  Created by Abdul on 31/10/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import UIKit

class WebserviceHandler: NSObject {

    // MARK: - Shared Instance Declaration
    static let sharedInstance = WebserviceHandler()
    
    // MARK: - Get Airports
    func getAirportsData(onCompletion: @escaping(Bool, responseJSON?, String) -> Void) {
        
        if Reachability .isConnectedToNetwork() {
            
            WebserviceHelper .sharedInstance .getNearByAirports(onCompletion: {
                json, error, data in
                
                DispatchQueue .main .async {
                    if json .hasKey() {
                        
                        guard let responseData = data else {
                            return
                        }
                        
                        var responseJSONObj: responseJSON?
                        
                        do {
                            responseJSONObj = try JSONDecoder() .decode(responseJSON .self, from: responseData)
                            onCompletion(true, responseJSONObj, .sAlertAPISuccess)
                        } catch (let error) {
                            onCompletion(false, responseJSONObj, error .localizedDescription)
                        }
                    } else {
                        onCompletion(false, nil, .sAlertAPIFailed)
                    }
                }
            })
            
        } else {
            DispatchQueue .main .async {
                onCompletion(false, nil, .sAlertNoInternet)
            }
        }
    }
}
