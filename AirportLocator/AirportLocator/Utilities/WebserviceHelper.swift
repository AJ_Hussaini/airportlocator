//
//  WebserviceHelper.swift
//  AirportLocator
//
//  Created by Abdul on 31/10/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import UIKit

class WebserviceHelper: NSObject {
    
    // MARK: - Shared Instance Declaration
    static let sharedInstance = WebserviceHelper()
    
    // MARK: - Identifiers
    let airportQueryParams: [String: String] = [
        .sAirportQueryParamLocationKey: "\(Constants .sharedInstance .getValueFromUserDefaults(key: .keyLatitude)),\(Constants .sharedInstance .getValueFromUserDefaults(key: .keyLongitude))",
        .sAirportQueryParamRadiusKey: "100000",
        .sAirportQueryParamTypeKey: .sAirportQueryParamValue,
        .sAirportQueryParamKeywordKey: .sAirportQueryParamValue,
        .sAirportQueryParamKey: .sGoogleAPIKey
    ]
    
    // MARK: - GET API METHOD
    func makeHTTPGetRequest(onCompletion: @escaping(NSDictionary, NSError?, Data?) -> Void) {
        
        //  URL components
        var urlComponents = URLComponents()
        urlComponents .scheme = .sAPIURLScheme
        urlComponents .host = .sAPIURLHost
        urlComponents .path = .sAPIAirportPath
        urlComponents .setQueryItems(with: airportQueryParams)
        
        guard let url = urlComponents .url else {
            onCompletion([:], nil, nil)
            return
        }
        
        //  URL request
        let urlRequest = NSMutableURLRequest(url: url)
        urlRequest .addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //  URL session
        let urlSession = URLSession .shared
        let urlTask = urlSession .dataTask(with: urlRequest as URLRequest, completionHandler: {
            data, response, error -> Void in
            
            var json: NSDictionary = [:]
            
            if data != nil {
                do {
                    json = try JSONSerialization .jsonObject(with: data!, options: []) as! NSDictionary
                } catch {
                    // Do nothing for now
                }
            }
            onCompletion(json, error as NSError?, data)
        })
        urlTask .resume()
    }
    
    // MARK: - Get Nearby Airports API
    func getNearByAirports(onCompletion: @escaping(NSDictionary, NSError?, Data?) -> Void) {
        makeHTTPGetRequest(onCompletion: {
            json, error, data in
            DispatchQueue .main .async {
                onCompletion(json as NSDictionary, error, data)
            }
        })
    }
}
