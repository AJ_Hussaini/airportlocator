//
//  HomeController.swift
//  AirportLocator
//
//  Created by Abdul on 30/10/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import UIKit
import MapKit

class HomeController: UIViewController {
    
    // MARK: - Identifiers
    var airportsArtwork: [MapArtWork] = []
    
    // MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView?
    
    // MARK: - UIView Life Cycle Methods

    override func viewDidLoad() {
        self .navigationItem .setHidesBackButton(true, animated: true)
        self .navigationItem .title = "Airport Locator"
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureInitialMapView()
        getAirports()
    }
    
    // MARK: - Configure Initial Map View Methods
    
    func configureInitialMapView() {
        //  To point user's current location
        let initialLocation = CLLocation(latitude: Constants .sharedInstance .getValueFromUserDefaults(key: .keyLatitude) as? Double ?? 0.0, longitude: Constants .sharedInstance .getValueFromUserDefaults(key: .keyLongitude) as? Double ?? 0.0)
        centerMapOnLocation(location: initialLocation)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        //  Configured visible region of user's location around 100 KM
        let coordinateRegion = MKCoordinateRegion(center: location .coordinate, latitudinalMeters: 100000, longitudinalMeters: 100000)
        mapView? .setRegion(coordinateRegion, animated: true)
        
        let artWork = MapArtWork(locationName: "You are here!", locationIcon: "", coordinate: location .coordinate)
        mapView? .addAnnotation(artWork)
    }
    
    // MARK: - Get Airports API Call Methods
    
    func getAirports() {
        WebserviceHandler .sharedInstance .getAirportsData(onCompletion: {
            responseStatus, responseData, responseMessage in
            if responseStatus {
                self .responseHandler(airports: responseData? .results ?? [])
            } else {
                let alert = UIAlertController(title: "Alert", message: responseMessage, preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert .addAction(alertAction)
                self .present(alert, animated: true, completion: nil)
            }
        })
    }
    
    // MARK: - Handle API Response Data Methods
    
    func responseHandler(airports: [airportData]) {
        if airports .count > 0 {
            self .airportsArtwork .removeAll()
            for airport in airports {
                // Customized map art work
                let mapArtWork = MapArtWork()
                mapArtWork .locationName = airport .name
                mapArtWork .locationIcon = airport .icon
                mapArtWork .coordinate = CLLocationCoordinate2D(latitude: airport .geometry? .location? .lat ?? 0.0, longitude: airport .geometry? .location? .lng ?? 0.0)
                self .airportsArtwork .append(mapArtWork)
            }
            self .mapView? .addAnnotations(self .airportsArtwork)
        }
    }
}

extension HomeController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? MapArtWork else {
            return nil
        }
        
        let annotationIdentifier = "marker"
        var annotationView: MKMarkerAnnotationView
        
        if let dequeueView = mapView .dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) as? MKMarkerAnnotationView {
            dequeueView .annotation = annotation
            annotationView = dequeueView
            
        } else {
           annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView .canShowCallout = true
            annotationView .calloutOffset = CGPoint(x: -5, y: 5)
            annotationView .rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view .annotation as! MapArtWork
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location .mapItem() .openInMaps(launchOptions: launchOptions)
    }
}
