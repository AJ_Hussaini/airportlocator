//
//  OnBoardingController.swift
//  AirportLocator
//
//  Created by Abdul on 30/10/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import UIKit

class OnBoardingController: UIViewController {
    
    // MARK: - Identifiers
    // MARK: - IBOutlets
    @IBOutlet weak var lblEnableLocationAccess: UILabel?
    
    // MARK: - UIView Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter .default .addObserver(self, selector: #selector(gotLocationUpdate(notification:)), name: .locationAccess, object: nil)
        
        LocationManagerHandler .sharedInstance .getCurrentLocation()
    }
    
    // MARK: - Notification Center Handler
    
    @objc func gotLocationUpdate(notification: NSNotification) {
        let isLocationEnabled = Constants .sharedInstance .getValueFromUserDefaults(key: .keyisLocationEnabled) as? Bool ?? false
        if isLocationEnabled {
            //  Move to home view controller
            lblEnableLocationAccess? .isHidden = true
            if let homeController: HomeController = UIStoryboard .init(name: "Main", bundle: nil) .instantiateViewController(withIdentifier: "HomeController") as? HomeController {
                NotificationCenter .default .removeObserver(self, name: .locationAccess, object: nil)
                self .navigationController? .pushViewController(homeController, animated: false)
            }
        } else {
            lblEnableLocationAccess? .isHidden = false
            lblEnableLocationAccess? .text = .sEnableLocationAccess
        }
    }
}
