//
//  AirportModel.swift
//  AirportLocator
//
//  Created by Abdul on 30/10/2019.
//  Copyright © 2019 AbdulJabbar.S @ AJ. All rights reserved.
//

import Foundation

// MARK: - Response JSON Data
struct responseJSON: Codable {
    
    var status: String?
    var results: [airportData]?
    
    init(status: String? = nil,
         results: [airportData]? = nil) {
        self .status = status ?? ""
        self .results = results ?? []
    }
}

// MARK: - Airport Data
struct airportData: Codable {
    
    var geometry: geometryData?
    var icon: String?
    var id: String?
    var name: String?
    
    init(geometry: geometryData? = nil,
         icon: String? = nil,
         id: String? = nil,
         name: String? = nil) {
        
        self .geometry = geometry ?? geometryData()
        self .icon = icon ?? ""
        self .id = id ?? ""
        self .name = name ?? ""
    }
}

// MARK: - Geometry Data
struct geometryData: Codable {
    
    var location: locationData?
    var viewport: viewPortData?
    
    init(location: locationData? = nil,
         viewport: viewPortData? = nil) {
        
        self .location = location ?? locationData()
        self .viewport = viewport ?? viewPortData()
    }
}

// MARK: - View Port Data
struct viewPortData: Codable {
    
    var northeast: northEastData?
    var southwest: southWestData?
    
    init(northeast: northEastData? = nil,
         southwest: southWestData? = nil) {
        
        self .northeast = northeast ?? northEastData()
        self .southwest = southwest ?? southWestData()
    }
}

// MARK: - Southwest Data
struct southWestData: Codable {
    
    var lat: Double?
    var lng: Double?
    
    init(lat: Double? = nil,
         lng: Double? = nil) {
        
        self .lat = lat ?? 0.0
        self .lng = lng ?? 0.0
    }
}

// MARK: - Northeast Data
struct northEastData: Codable {
    
    var lat: Double?
    var lng: Double?
    
    init(lat: Double? = nil,
         lng: Double? = nil) {
        
        self .lat = lat ?? 0.0
        self .lng = lng ?? 0.0
    }
}

// MARK: - Location Data
struct locationData: Codable {
    
    var lat: Double?
    var lng: Double?
    
    init(lat: Double? = nil,
         lng: Double? = nil) {
        
        self .lat = lat ?? 0.0
        self .lng = lng ?? 0.0
    }
}
